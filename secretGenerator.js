const fs = require('fs');
const {promisify} = require('util');

async function createAuthTxt(){
	try {
		const randNumber = await generateRandNumber()
		return await writeToTxt(randNumber.toString())
	}
	catch (err) {
		console.log(err)
		return {"status":"failed","message":err}
	}
}

async function writeToTxt(content){
	try {
		const writeFileAsync = promisify(fs.writeFile)
		const buffer = new Uint8Array(Buffer.from(content))
		return await writeFileAsync('secret.txt', buffer)
	}
	catch (err){
		console.log(err)
		return err
	}
}

async function generateRandNumber(){
	try {
		const randNumber = await Math.floor(
			Math.random() * (83272 - 100 + 1) + 100	
		) 
		console.log(randNumber)
		return randNumber
	}
	catch (err){
		console.log(err)
		return err
	}
}


module.exports = {
	createAuthTxt: createAuthTxt
}


