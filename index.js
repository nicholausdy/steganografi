const http = require('http')
const fs = require('fs') //filesystem read
const handler = require ('./handler')
const express = require('express'),
	bodyParser = require('body-parser');
const app = express()
const perf = require('execution-time')();
app.use(bodyParser.json({limit:'1mb'}));
app.use(bodyParser.urlencoded({
	limit:'10mb',
	extended: true
}));

app.get('/main', async(req,res) => {
	try {
		perf.start()
		await handler.ordinaryHandler(req,res)
	}
	catch (err){
		console.log(err)
		res.json({"status":"failed","message":"Internal server error"})
	}
	finally {
		const execution = perf.stop()
		console.log('Execution time: '+execution.time);
	}
})

app.get('/main/:number', async(req,res) => {
	try {
		perf.start()
		await handler.steganoHandler(req,res)
	}
	catch (err){
		console.log(err)
		res.json({"status":"failed","message":"Internal server error"})
	}
	finally {
		const execution = perf.stop()
		console.log('Execution time: '+execution.time);
	}
})

http.createServer(app).listen(3001, () => {
	console.log('Maid cafe serving at port 3001')
    console.log('Ctrl+C to Terminate Process')
});
