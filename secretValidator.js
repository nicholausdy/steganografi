const fs = require('fs');
const {promisify} = require('util');
const secretGenerator = require('./secretGenerator.js')

async function validateAuthTxt(content){
	try {
		const key = await readTxt()
		if (key != content) {
			await secretGenerator.createAuthTxt()
			return false
		}
		else {
			await secretGenerator.createAuthTxt()
			return true
		}
	}
	catch (err){
		console.log(err)
		return {"status":"failed","message":"Unknown error encountered"}
	}
}

async function readTxt(){
	try {
		const readFileAsync = promisify(fs.readFile)
		return await readFileAsync('secret.txt','utf8')
	}
	catch (err){
		console.log(err)
		return err
	}
}

module.exports = {
	validateAuthTxt : validateAuthTxt
}

