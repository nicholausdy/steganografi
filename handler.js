const secretGenerator = require('./secretGenerator')
const secretValidator = require('./secretValidator')
const path = require('path')

async function ordinaryHandler(req,res){
	try {
		await res.sendFile(path.join(__dirname+'/login_mhs.html'))
	}
	catch (err) {
		console.log(err)
		return err
	}
}

async function steganoHandler(req,res){
	try {
		const isParamsValid = await secretValidator.validateAuthTxt(req.params.number)
		if (!isParamsValid) {
			await res.sendFile(path.join(__dirname+'/login_mhs.html'))
		}
		else {
			await res.sendFile(path.join(__dirname+'/login_mhs_2.html'))
		}
	}
	catch (err) {
		console.log(err)
		return err
	}
}

module.exports = {
	ordinaryHandler : ordinaryHandler,
	steganoHandler : steganoHandler
}